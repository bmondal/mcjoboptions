from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraphUtils as mgu
import os,subprocess,fileinput

#from MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment import *

# General settings
minevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
gridpack_mode=True

# MG merging settings
maxjetflavor=4
ickkw=0
required_accuracy = 0.0001
dyn_scale = '10'

name='tllq'
keyword=['SM','singleTop','tZ','lepton']

get_param_card = subprocess.Popen(['get_files','-data','aMcAtNlo_param_card_tt.dat'])
get_param_card.wait()


if not mgu.is_gen_from_gridpack():
    process = """
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t b~ j l+ l- $$ W+ W- [QCD] \n
    add process p p > t~ b j l+ l- $$ W+ W- [QCD]
    output -f
    """
    
    process_dir = mgu.new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION

# Decay with MadSpin
madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#set seed %i
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~

decay t > w+ b, w+ > l+ vl
decay t~ > w- b~, w- > l- vl~
decay z > l+ l-
launch """)
mscard.close()


pdflabel="lhapdf"
lhaid=260400
#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version'  : '3.0',
           'pdlabel'      : "'"+pdflabel+"'",
           'lhaid'        : lhaid,
           'maxjetflavor' : maxjetflavor,
           'ickkw'        : 0,
           'ptj'          : 0,
           'drll'         : 0.0,
           'ptl'          : 0,
           'etal'         : 10.0,
           'etaj'         : 10.0,
           'bwcutoff'      : 50,          
           'dynamical_scale_choice' : dyn_scale,
           'reweight_scale': 'True',
           'reweight_PDF': 'True',
           'rw_rscale': "1.0, 0.5, 2.0",
           'rw_fscale': "1.0, 0.5, 2.0",
           'PDF_set_min': 260401,
           'PDF_set_max':260500,
           'store_rwgt_info':True,
           'parton_shower': "HERWIGPP",
	   'nevents':minevents,
 }

scalefact = 1.0

# Cook the setscales file for the user defined dynamical scale
fileN = process_dir+'/SubProcesses/setscales.f'

mark = '      elseif(dynamical_scale_choice.eq.10) then'
rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
           'cc      to use this code you must set                                            cc',
           'cc                 dynamical_scale_choice = 10                                   cc',
           'cc      in the run_card (run_card.dat)                                           cc',
           'ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           'write(*,*) "User-defined scale not set"',
           'stop 1',
           'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
           'tmp = 0d0']

flag=0
for line in fileinput.input(fileN, inplace=1):
    toKeep = True
    for rmLine in rmLines:
        if line.find(rmLine) >= 0:
           toKeep = False
           break
    if toKeep:
        print line,
    if line.startswith(mark) and flag==0:
        flag +=1
        print """
c         Q^2= mb^2 + 0.5*(pt^2+ptbar^2)
c          rscale=0d0             !factorization scale**2 for pdf1
c                                  !factorization scale**2 for pdf2
c          xmtc=dot(P(0,6),P(0,6))
c          rscale = 4d0 * sqrt(pt(P(0,6))**2+xmtc)
          do i=3,nexternal
            xm2=dot(pp(0,i),pp(0,i))
            if ( xm2 < 30 .and. xm2 > 10 ) then
              tmp = 4d0 * dsqrt(pt(pp(0,i))**2+xm2)
c write(*,*) i, pt(P(0,i))**2, xmtc, sqrt(pt(P(0,i))**2+xmtc), rscale
c write(*,*) i, xmtc, pt(P(0,i)), rscale
            endif
          enddo
		""" 

mgu.modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

# Set up batch jobs
mgu.modify_config_card(process_dir=process_dir)#,settings={'cluster_type':'condor','cluster_queue':'None','run_mode':1})
mgu.generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs,required_accuracy=required_accuracy)

outputDS = mgu.arrange_output(process_dir=process_dir,runArgs=runArgs)


## Shower

evgenConfig.nEventsPerJob = 10000
evgenConfig.description = 'aMC@NLO_'+str(name)
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
evgenConfig.description = 'aMC@NLO+Herwig7  single top + Z production'
evgenConfig.keywords    = [ 'SM','singleTop','tZ','lepton','NLO']
evgenConfig.contact     = ['muhammad.alhroob@cern.ch' ]
evgenConfig.tune = "H7.1-Default"
runArgs.inputGeneratorFile=outputDS+".events"

check_reset_proc_number(opts)
include("Herwig7_i/Herwig72_LHEF.py")

Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118", max_flav=4)
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.run()

