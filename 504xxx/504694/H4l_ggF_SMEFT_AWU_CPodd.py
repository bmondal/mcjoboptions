# Based on: https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/501xxx/501013/mc.MGPy8EG_SMEFT_ggFH4l_SM.py

from MadGraphControl.MadGraphUtils import *
import os
from math import sqrt

madgraph_models_path = "/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/"
os.environ['DATAPATH'] = madgraph_models_path + ":" + os.environ['DATAPATH']

# This file is a configuration file.
# It is meant to be included in a joboption file, which should define the _PARAMS variable.
# It should be a map with keys corresponding to SMEFT couplings (cHWtil, cHBtil, cHWBtil).

#------------------------------------------------------------------------------
# require beam energy to be set as argument
#------------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")


#---------------------------------------------------------------------------------------------------
#General Settings
#---------------------------------------------------------------------------------------------------

safe_factor = 2.0 # there is about of 40% dropped by Pythia, take at least 1.4!
nevents = safe_factor * (runArgs.maxEvents if runArgs.maxEvents > 0 else evgenConfig.nEventsPerJob)

# gridpack_dir='madevent/'
# gridpack_mode=True
gridpack_dir=None
gridpack_mode=False


#---------------------------------------------------------------------------------------------------
# generating process cards
#---------------------------------------------------------------------------------------------------

# fcard = open('proc_card_mg5.dat','w')


# masses = {
#         '15': '1.777000e+00',
#         '25': '1.250000e+02',
#         '24': '80.387000',
#         '9000003': '80.387000',
#         '9000004': '80.387000',
#         '251': '80.387000',
#         'DECAY  25': '4.995000e-03',
#         }
# yukawa = { '15': '1.777000e+00' }


my_process = """
import model SMEFTsim_A_U35_MwScheme_UFO-massless_cHWtil_cHBtil_cHWBtil_SMEFT_H4l
set gauge unitary

define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define jb = g u c d s b u~ c~ d~ s~ b~
generate p p > h  QED=1 NP=1, h > l+ l- l+ l- NP=1 @0
add process p p > h jb QED=1 NP=1, h > l+ l- l+ l- NP=1 @1
add process p p > h jb jb  QED=1 NP=1, h > l+ l- l+ l- NP=1 @2

output -f
"""

process_dir = new_process( my_process )


#------------------------------------------------------------------------------
# creating param card, based on restricted model
#------------------------------------------------------------------------------

template_param_card = "restrict_massless_cHWtil_cHBtil_cHWBtil_SMEFT_H4l.dat"
modify_param_card(
    process_dir=process_dir,
    param_card_input = template_param_card,
    param_card_backup = template_param_card + ".bak",
    params = {'FRBLOCK': _PARAMS}
)



#---------------------------------------------------------------------------------------------------
# creating run_card.dat for ggF
#---------------------------------------------------------------------------------------------------

# ktdurham cut
ktdurham = 40

extras = {
    'lhe_version'  : '3.0',
    'pdlabel'      : "'nn23lo1'",
    'lhaid'        : 230000,
    #'parton_shower'  :'PYTHIA8',
    'event_norm'   : 'sum',
    'cut_decays'   : 'T',
    'pta'          : 0.,
    'ptl'          : 0.,
    'etal'         : -1.0,
    'drjj'         : 0.0,
    'draa'         : 0.0,
    'etaj'         : -1.0,
    'draj'         : 0.0,
    'drjl'         : 0.0,
    'dral'         : 0.0,
    'etaa'         : -1.0,
    'drll'         : 0.05,
    'ktdurham'     : ktdurham,
    'auto_ptj_mjj' : "False",
    'ickkw'        : 0,
    'xqcut'        : 0.0,
    'dparameter'   : 0.4,
    'ptj'          : 10.,
    'ptj1min'      : 0,
    'ptj1max'      : -1.0,
    'mmjj'         : 3.,
    'mmjjmax'      : -1.0,
    'nevents'      : int(nevents),
}

modify_run_card(process_dir=process_dir,
                runArgs=runArgs,
                settings=extras)


#------------------------------------------------------------------------------
# run MadGraph
#------------------------------------------------------------------------------

print_cards()

generate(process_dir=process_dir,
         runArgs=runArgs)


#-------------------------------------------------------------------------------
# Multi-core capability
#-------------------------------------------------------------------------------
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  '
                  'Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

arrange_output(process_dir=process_dir,
               runArgs=runArgs,
               lhe_version=3,
               saveProcDir=True)


evgenConfig.description = "ggF 125 GeV Higgs CKKW-L merged jets production in the SMEFT model decaying to zz4l."
evgenConfig.keywords = ['Higgs', 'gluonFusionHiggs', 'BSM', 'mH125', 'jets']
evgenConfig.contact = ['Jiawei Wang <jiawei.wang@cern.ch>', 'Antoine Laudrain <antoine.laudrain@cern.ch>']


#---------------------------------------------------------------------------------------------------
# CKKW-L jet matching
#---------------------------------------------------------------------------------------------------

PYTHIA8_nJetMax=2
PYTHIA8_Dparameter=float(extras['dparameter'])
#PYTHIA8_Process=my_process
#PYTHIA8_Process="guess"
PYTHIA8_Process="pp>h"
PYTHIA8_TMS=float(extras['ktdurham'])
PYTHIA8_nQuarksMerge=4

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
genSeq.Pythia8.Commands+=["Merging:mayRemoveDecayProducts=on"]
# modification of merging to allow pythia to guess the hard process with "guess" syntax
#if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    #genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
#else:
    #genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'

