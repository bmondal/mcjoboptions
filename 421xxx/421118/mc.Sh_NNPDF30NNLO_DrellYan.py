## Base config for Sherpa
from Sherpa_i.Sherpa_iConf import Sherpa_i
genSeq += Sherpa_i()

## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
fixSeq.FixHepMC.LoopsByBarcode = False

## Disable TestHepMC for the time being, cf.
## https://its.cern.ch/jira/browse/ATLMCPROD-1862
if hasattr(testSeq, "TestHepMC"):
    testSeq.remove(TestHepMC())

genSeq.Sherpa_i.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
genSeq.Sherpa_i.Parameters += [ 'LOG_FILE=sherpa.log' ]
genSeq.Sherpa_i.Parameters += [
         "MAX_PROPER_LIFETIME=10.0",
         "HEPMC_TREE_LIKE=1",
         "PRETTY_PRINT=Off",
         ]

evgenConfig.generators = [ "Sherpa" ]
evgenConfig.tune = "NNPDF3.0 NNLO"
evgenConfig.description = "Sherpa example JO, Z+0,1-jet production."
evgenConfig.keywords = [ "drellYan" ]
evgenConfig.contact  = [ "chris.g@cern.ch" ]

evgenConfig.process="""
(run){
  % beam setup
  BEAM_1=2212; BEAM_2=2212;
  BEAM_ENERGY_1=6500; BEAM_ENERGY_2=6500;
  RESULT_DIRECTORY=Results

  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=0; LJET:=0; QCUT:=20.;
}(run)

(processes){
  Process 93 93 -> 11 -11 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  End process;
}(processes)

(selector){
  Mass 11 -11 40 E_CMS
}(selector)
"""
