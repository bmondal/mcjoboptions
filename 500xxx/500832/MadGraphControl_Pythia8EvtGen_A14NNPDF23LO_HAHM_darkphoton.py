import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

import os
### get MC job-options filename
FIRST_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
jofiles = [f for f in os.listdir(FIRST_DIR) if (f.startswith('mc') and f.endswith('.py'))]

#---------------------------------------------------------------------------
# General Settings
#---------------------------------------------------------------------------
cluster_type="condor"
cluster_queue=None

gridpack_mode=False

## safe factor applied to nevents, to account for the filter efficiency
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
nevents = int(nevents)

#---------------------------------------------------------------------------
# Process
#---------------------------------------------------------------------------

isOnshell= (MHSinput > 2*mZDinput) ## dark Higgs decays to two on-shell dark photons
isNonPrompt= (MHSinput < mZDinput)
proc_str=""  ## for the production of dark Higgs and dark photon
decay_str="" ## decay
if isOnshell:
    proc_str="generate p p > z > zp h2, h2 > zp zp"
    decay_str="decay zp > fall fall; decay zp > l l; decay zp > l l" 
else:
    proc_str="generate p p > z > zp h2"
    if isNonPrompt:
        decay_str="decay zp > l l"
    else:
        decay_str="decay h2 > zp fall fall, zp > l l; decay zp > l l"

if not is_gen_from_gridpack():
    process = """
    import model HAHM_darkphoton_iDM_UFO
    define p = u c s d b u~ c~ s~ d~ b~ g
    define j = u c s d b u~ c~ s~ d~ b~ g
    define fall = u c s d b u~ c~ s~ d~ b~ e- e+ ta- ta+ mu- mu+ ve vm vt ve~ vm~ vt~
    %s
    output -f
    """ % proc_str
    
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else: 
  raise RuntimeError("No center of mass energy found.")

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
safefactor = 2.0
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents)*safefactor
else: nevents = nevents*safefactor
skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 run Card
#---------------------------------------------------------------------------

extras = {
    'lhe_version': "3.0",
    'cut_decays' : 'F',
    'nevents'    : nevents,
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

# Set up batch jobs
if gridpack_mode and (not is_gen_from_gridpack()):
  modify_config_card(process_dir=process_dir,settings={'mode':'0'})


#---------------------------------------------------------------------------
# MG5 param Card
#---------------------------------------------------------------------------

## params is a dictionary of dictionaries (each dictionary is a 
## separate block)
params={}
mhd={}
mhd['mZDinput']=mZDinput
mhd['MHSinput']=MHSinput
mhd['epsilon']="1.000000e-03" ## default
mhd['kap']="1.000000e-09" ## default
mhd['aXM1']="1.000000e+01" ## default
params['hidden']=mhd

decays={
  '35': 'DECAY  35 Auto',
  #'1023': 'DECAY  1023 Auto',
}
params['DECAY']=decays

modify_param_card(process_dir=process_dir,params=params)


#---------------------------------------------------------------------------
# creating mad spin card
#---------------------------------------------------------------------------

madspin_card_loc='madspin_card.dat'
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')                                                                                               

mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
set spinmode none
define fall = u c s d b u~ c~ s~ d~ b~ e- e+ ta- ta+ mu- mu+ ve vm vt ve~ vm~ vt~
define l = mu- mu+ e- e+
%s
# running the actual code
launch"""% (runArgs.randomSeed, decay_str) )

mscard.close()


#---------------------------------------------------------------------------
# MG5 process (lhe) generation
#---------------------------------------------------------------------------

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

#---------------------------------------------------------------------------
# Lifetime setting for non-prompt
#---------------------------------------------------------------------------
if isNonPrompt:
    # initialise random number generator/sequence
    import random
    random.seed(runArgs.randomSeed)
    # lifetime function
    def lifetime(avgtau = 21):
        import math
        t = random.random()
        return -1.0 * avgtau * math.log(t)
    
    # replacing lifetime of scalar, manually
    MGDIR=os.environ['MADPATH']
    sys.path.append(MGDIR)
    from madgraph.various.lhe_parser import *
    from math import fabs
    
    rname = "run_01"
    # use the decayed LHE file
    madspinDirs=sorted(glob.glob(process_dir+'/Events/'+rname+'_decayed_*/'))
    if len(madspinDirs)==0:
        lhedir = process_dir+'/Events/'+rname
    else:
        lhedir = madspinDirs[-1]
    oldlhe = lhedir+'/unweighted_events.lhe.gz'
    newlhe = open(lhedir+'/unweighted_events2.lhe','w')
    # read old lhe file
    elhe_old = EventFile(oldlhe)
    # banner
    banner=elhe_old.banner
    newlhe.write(banner)
    # process events
    for event in elhe_old:
        
        dict_lt={}
        for particle in event:
            ## dark Higgs
            if fabs(particle.pid) in [35]:
                lt = lifetime(avgtau)
                dict_lt[int(event.index(particle))]=lt
                particle.vtim = lt # distance travelled by the particle.
            ## fermions from the dark Higgs decay
            else:
                mother = getattr(particle, "mother1")
                if isinstance(mother, Particle):
                    if fabs(mother.pid) in [35]:
                        lt = dict_lt[int(event.index(mother))]
                        particle.vtim = lt # same life of its mother
                else:
                    indm=int(mother)
                    pmother=event[indm]
                    if fabs(pmother.pid) in [35]:
                        lt = dict_lt[indm]
                        particle.vtim = lt # same life of its mother
            
        # write new event
        newlhe.write(str(event))
    # close new file
    newlhe.write('</LesHouchesEvent>\n')
    newlhe.close()
    
    zip1 = subprocess.Popen(['gzip', lhedir+'/unweighted_events2.lhe'])
    zip1.wait()
    shutil.move(lhedir+'/unweighted_events2.lhe.gz', lhedir+'/unweighted_events.lhe.gz')
    # done
#---------------------------------------------------------------------------


arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Helper for resetting process number
check_reset_proc_number(opts)


#---------------------------------------------------------------------------
# Shower
#---------------------------------------------------------------------------

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

if isNonPrompt:
    if decaytype==0: ## leptonic
        genSeq.Pythia8.Commands += [
                            '35:onMode = off', # decay of the dark Higgs
                            '35:oneChannel = on 0.5 100 11 -11',
                            '35:addChannel = on 0.5 100 13 -13',
                            '35:m0 = %.1f' % MHSinput, # mass
                            '35:mMin = 0',
                            ]
    elif decaytype==1: ## hadronic (TODO: use more reastic branching ratio)
        genSeq.Pythia8.Commands += [
                            '35:onMode = off', # decay of the dark Higgs
                            '35:oneChannel = on 0.67 100 5 -5',
                            '35:addChannel = on 0.33 100 4 -4',
                            '35:m0 = %.1f' % MHSinput, # mass
                            '35:mMin = 0',
                            ]

    genSeq.Pythia8.Commands = [i for i in genSeq.Pythia8.Commands if (("limitTau0" not in i) and ("tau0Max" not in i))]
    genSeq.Pythia8.Commands += [
                            'ParticleDecays:tau0Max = 100000.0',
                            'ParticleDecays:limitTau0 = off'
                           ]

    testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
    testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm

#--------------------------------------------------------------
# 4l pt>2GeV Eta<2.8 filters
#--------------------------------------------------------------
include("GeneratorFilters/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut = 2000.
filtSeq.MultiLeptonFilter.Etacut = 3.0
if isNonPrompt:
    if decaytype==0: ## leptonic
      filtSeq.MultiLeptonFilter.NLeptons = 4
    elif decaytype==1: ## hadronic
      filtSeq.MultiLeptonFilter.NLeptons = 2
else:
    filtSeq.MultiLeptonFilter.NLeptons = 4

#--------------------------------------------------------------
# Evgen
#--------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = "Test HAHM_darkphoton_iDM_darkhiggs_UFO"
evgenConfig.keywords = ["exotic","BSM"]
evgenConfig.process = "p p > zp h2"
evgenConfig.contact = ["Mingyi Liu <mingyi.liu@cern.ch>"]
