include("PowhegControl_ttFCNC_NLO.py")

# Higgs decay
Herwig7Config.add_commands ("""
do /Herwig/Particles/h0:SelectDecayModes h0->tau-,tau+;
do /Herwig/Particles/h0:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()
