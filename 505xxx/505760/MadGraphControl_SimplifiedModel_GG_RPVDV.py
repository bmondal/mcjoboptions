from MadGraphControl.MadGraphUtils import *

include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')
##include ( './SUSY_SimplifiedModel_PreInclude.py')

nevents = runArgs.maxEvents*1.3 if runArgs.maxEvents>0 else 1.3*evgenConfig.nEventsPerJob
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
#print("z_phys_short")
#print(phys_short)

#param_card = 'param_card.SM.GG.direct.dat' 

#print("z_param_card")
#print(param_card)


#frblock = {}
masses = {}
decays = {}

#joparts = runArgs.jobConfig[0].rstrip('.py').split('_')
joparts = get_physics_short().split('_')

masses['1000021'] = float(joparts[4])
if masses['1000021'] < 1900:
    nevents = runArgs.maxEvents*1.5 if runArgs.maxEvents>0 else 1.5*evgenConfig.nEventsPerJob
masses['1000022'] = float(joparts[5])
if masses['1000022']<0.5: masses['1000022']=0.5
gentype = joparts[2]
decaytype = '_'.join([joparts[3],joparts[6],joparts[7]]) 
short_decaytype = joparts[3]
lambda_coupling = joparts[6]
neutralino_ctau = float(joparts[7].replace('p','.')) 
neutralino_width = 3.*1e11*6.581e-25/neutralino_ctau
if lambda_coupling == "lamp211":
    decays = {'1000022':"""DECAY   1000022  """   +str(neutralino_width)+  """ # neutralino decay
    ##           BR         NDA      ID1       ID2       ID3
    2.5000000E-01    3     13        2         -1   # BR(~chi_10 -> mu udbar)
    2.5000000E-01    3    -13       -2          1   # BR(~chi_10 -> mubar ubard)
    2.5000000E-01    3     14        1         -1   # BR(~chi_10 -> nu ddbar)
    2.5000000E-01    3    -14       -1          1   # BR(~chi_10 -> nubar dbard) """}
elif lambda_coupling == "lamp111":
    decays = {'1000022':"""DECAY   1000022  """   +str(neutralino_width)+  """ # neutralino decay
    ##           BR         NDA      ID1       ID2       ID3
    2.5000000E-01    3     11        2         -1   # BR(~chi_10 -> e udbar)
    2.5000000E-01    3    -11       -2          1   # BR(~chi_10 -> ebar ubard)
    2.5000000E-01    3     12        1         -1   # BR(~chi_10 -> nu ddbar)
    2.5000000E-01    3    -12       -1          1   # BR(~chi_10 -> nubar dbard) """}
elif lambda_coupling == "lamp223":
    decays = {'1000022':"""DECAY   1000022  """   +str(neutralino_width)+  """ # neutralino decay
    ##           BR         NDA      ID1       ID2       ID3
    2.5000000E-01    3     13        4         -5   # BR(~chi_10 -> mu cbbar)
    2.5000000E-01    3    -13       -4          5   # BR(~chi_10 -> mubar cbarb)
    2.5000000E-01    3     14        4         -5   # BR(~chi_10 -> nu bbarb)
    2.5000000E-01    3    -14       -4          5   # BR(~chi_10 -> nubar bbbar) """}
elif lambda_coupling == "lamp123":
    decays = {'1000022':"""DECAY   1000022  """   +str(neutralino_width)+  """ # neutralino decay
    ##           BR         NDA      ID1       ID2       ID3
    2.5000000E-01    3     11        4         -5   # BR(~chi_10 -> e cbbar)
    2.5000000E-01    3    -11       -4          5   # BR(~chi_10 -> ebar cbarb)
    2.5000000E-01    3     12        4         -5   # BR(~chi_10 -> nu bbarb)
    2.5000000E-01    3    -12       -4          5   # BR(~chi_10 -> nubar bbbar) """}
elif lambda_coupling == "lamp213":
    decays = {'1000022':"""DECAY   1000022  """   +str(neutralino_width)+  """ # neutralino decay
    ##           BR         NDA      ID1       ID2       ID3
    2.5000000E-01    3     13        2         -5   # BR(~chi_10 -> mu ubbar)
    2.5000000E-01    3    -13       -2          5   # BR(~chi_10 -> mubar ubarb)
    2.5000000E-01    3     14        1         -5   # BR(~chi_10 -> nu dbarb)
    2.5000000E-01    3    -14       -1          5   # BR(~chi_10 -> nubar dbbar) """}    
elif lambda_coupling == "lamp113":
    decays = {'1000022':"""DECAY   1000022  """   +str(neutralino_width)+  """ # neutralino decay
    ##           BR         NDA      ID1       ID2       ID3
    2.5000000E-01    3     11        2         -5   # BR(~chi_10 -> e ubbar)
    2.5000000E-01    3    -11       -2          5   # BR(~chi_10 -> ebar ubarb)
    2.5000000E-01    3     12        1         -5   # BR(~chi_10 -> nu dbarb)
    2.5000000E-01    3    -12       -1          5   # BR(~chi_10 -> nubar dbbar) """}
elif lambda_coupling == "lam12k":
    decays = {'1000022':"""DECAY   1000022  """   +str(neutralino_width)+  """ # neutralino decay
    ##           BR         NDA      ID1       ID2       ID3
    8.333333E-02    3      12       -13        11   # BR(~chi_10 -> nue mubar e)
    8.333333E-02    3     -12        13       -11   # BR(~chi_10 -> nuebar mu ebar)
    8.333333E-02    3      14       -13        11   # BR(~chi_10 -> numu mubar e)
    8.333333E-02    3     -14        13       -11   # BR(~chi_10 -> numubar mu ebar)
    1.666667E-01    3      14       -11        11   # BR(~chi_10 -> numu ebar e)
    1.666667E-01    3     -14        11       -11   # BR(~chi_10 -> numubar e ebar)
    1.666667E-01    3      12       -13        13   # BR(~chi_10 -> nue mubar mu)
    1.666667E-01    3     -12        13       -13   # BR(~chi_10 -> nuebar mu mubar) """}

# Set up the decays - gluino to four flavors of quarks
decays['1000021'] = """DECAY   1000021     7.40992706E-02   # gluino decays
#           BR         NDA      ID1       ID2       ID3
     2.50000000E-01    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.50000000E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.50000000E-01    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.50000000E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
"""


process = '''
import model RPVMSSM_UFO
define susysq = ul ur dl dr cl cr sl sr t1 t2 b1 b2
define susysq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate p p > go go / susysq susysq~ @1
add process p p > go go j / susysq susysq~ @2
add process p p > go go j j / susysq susysq~ @3
'''
njets = 2

#build_param_card(param_card_old='param_card.SM.GG.direct.dat', param_card_new='param_card.SM.GG.'+decaytype+'.dat',      masses=masses,decays=decays)

#process_dir = new_process(process)
#modify_param_card(param_card_input='param_card.SM.GG.direct.dat' process_dir=process_dir,params={'MASS':masses,'DECAY':decays})

evgenConfig.contact  = [ "zachary.pollock@cern.ch" ]
evgenConfig.keywords += ['gluino','SUSY','simplifiedModel','RPV', 'neutralino','longLived']
evgenConfig.description = 'gluino pair production with gluino -> qq+LSP, RPV scenarios with displaced neutralino decay in simplified model, m_glu = %s GeV, m_N1 = %s GeV, ctau(N1) = %s mm'%(masses['1000021'],masses['1000022'],neutralino_ctau)
#evgenConfig.minevents = 20000

randomSeed_original = 1234
if hasattr(runArgs, "randomSeed"):
    randomSeed_original = runArgs.randomSeed
# Giving a unique seed number (not exceeding the limit of ~30081^2)
#runArgs.randomSeed = 1000 * int(str(runArgs.runNumber)[1:6]) + randomSeed_original
runArgs.randomSeed = randomSeed_original

evt_multiplier = 1.3
if masses['1000021'] < 1900:
    nevents = runArgs.maxEvents*1.5 if runArgs.maxEvents>0 else 1.5*evgenConfig.nEventsPerJob

extras = {'time_of_flight':'0',
          'nevents':nevents}
add_lifetimes_lhe = True
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )
#include ( './SUSY_SimplifiedModel_PostInclude.py' )

runArgs.randomSeed = randomSeed_original

from MadGraphControl.MadGraphUtils import check_reset_proc_number
check_reset_proc_number(opts)

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{go,1000021}{go,1000021}"]

# Turn off checks for displaced vertices. 
# Other checks are fine.
testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #In mm
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000

