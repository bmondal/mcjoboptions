evgenConfig.nEventsPerJob = 2000
get_param_card = subprocess.Popen(['get_files', '-data', 'param_card.SM.SVT.leplep.dat'])
if get_param_card.wait():
        print "Could not get hold of param_card.SM.SVT.leplep.dat, exiting..."
        sys.exit(2)
get_run_card = subprocess.Popen(['get_files', '-data', 'run_card.dat'])
if get_run_card.wait():
        print "Could not get hold of run_card.dat, exiting..."
        sys.exit(2)
include("MadGraphControl_Py8EG_SVT_RPVleplep.py")
