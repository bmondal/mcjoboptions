'''
R21.6 job options for photon fusion production of chargino pairs

For useful links with helpful information, see:
  https://twiki.cern.ch/twiki/bin/view/AtlasProtected/SUSYMcRequestProcedureRel21
  https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYMcRequestProcedure
  https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/MadGraphControl_SimplifiedModel_C1C1_WW_MadSpin.py
  https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/param_card.SM.C1C1.WW.dat
  https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.54/Generators/MadGraphControl/share/common/SUSY_SimplifiedModel_PreInclude.py
'''
from MadGraphControl.MadGraphUtils import *

phys_short = get_physics_short()
def StringToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)

yytype = phys_short.split('_')[1]                # Type of photon fusion from job option name
mc1    = StringToFloat(phys_short.split('_')[4]) # Chargino mass
mn1    = StringToFloat(phys_short.split('_')[5]) # Neutralino mass

#-----------------------------------------
# Set masses in the parameter card 
#-----------------------------------------
# Following 
decoupled_mass = '4.5E9'
masses = {}
for p in ['1000001','1000002','1000003','1000004','1000005','1000006','2000001','2000002','2000003','2000004','2000005','2000006','1000021',\
          '1000023','1000024','1000025','1000011','1000013','1000015','2000011','2000013','2000015','1000012','1000014','1000016','1000022',\
          '1000035','1000037','35','36','37']: # Note that gravitino is non-standard
    masses[p]=decoupled_mass

# C1/N2 denegenerate 
masses['1000023'] = mc1
masses['1000024'] = mc1
masses['1000022'] = mn1   # N1_LSP

#-----------------------------------------
# Set decays in the parameter card 
#-----------------------------------------
decays = {}
# Specify chargino decays
decays['1000024']="""DECAY   1000024     1.70414503E-02   # chargino1+ decays
#          BR        NDA      ID1           ID2      ID3  
     1.00000000E+00   2     1000022          24          # BR(~chi_1+ -> ~chi_10  W+)
     0.00000000E+00   3     1000022         -11      12  # BR(~chi_1+ -> ~chi_10  e+ nu_e)
     0.00000000E+00   3     1000022         -13      14  # BR(~chi_1+ -> ~chi_10  mu+ nu_mu)
"""

# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
nevents=5*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
#mode=0

# Specify MadGraph process, leave Pythia to decay inclusively
process = '''
import MSSM_SLHA2
generate a a > x1+ x1- 
output -f
''' 

# Modify default MadGraph run_card.dat settings
run_settings = {'event_norm'      :'sum',
                'drjj'            :0.0,
                'lhe_version'     :'3.0',
                'cut_decays'      :'F',
                'use_syst'        :'F',
                'nevents'         :int(nevents)
                } 

if   'yyfusionEE' in yytype: 
  evgenLog.info('Detected yyfusionEE in job option: generate exclusive elastic (EE) photon fusion')
  run_settings['lpp1']            = '2' # 2 = photon initiated from proton EPA
  run_settings['lpp2']            = '2'
  run_settings['dsqrt_q2fact1']   =  2. 
  run_settings['dsqrt_q2fact2']   =  2.
  run_settings['fixed_fac_scale'] = 'T' 
elif 'yyfusionSD' in yytype: 
  evgenLog.info('Detected yyfusionSD in job option: generate single dissociative +z intact proton (SD) photon fusion')
  run_settings['lpp1']            = '1' # 1 = photon initiated from proton PDF
  run_settings['lpp2']            = '2' # 2 = photon initiated from proton EPA
  run_settings['dsqrt_q2fact2']   =  2.
elif 'yyfusionDS' in yytype: 
  evgenLog.info('Detected yyfusionDS in job option: generate single dissociative -z intact proton (DS) photon fusion')
  run_settings['lpp1']            = '2' # 2 = photon initiated from proton EPA
  run_settings['lpp2']            = '1' # 1 = photon initiated from proton PDF
  run_settings['dsqrt_q2fact1']   =  2. 
elif 'yyfusionDD' in yytype: 
  evgenLog.info('Detected yyfusionDD in job option: generate double dissociative (DD) photon fusion')
  run_settings['lpp1'] = '1' 
  run_settings['lpp2'] = '1'

# Metadata
evgenConfig.generators  = ["MadGraph"]
evgenConfig.contact     = [ "kristin.lohwasser@cern.ch", "jesse.liu@cern.ch" ]
evgenConfig.description = 'Photon fusion production of chargino pairs, direct decay W + neutralino dark matter'
evgenLog.info('a a > c1 c1 with m(c1, n1) = ({0}, {1}) GeV'.format(mc1, mn1))
evgenConfig.keywords   +=["SUSY", "Charginos", "Photon fusion production"]

#-------------------------------------------------------------- 
# Generate MadGraph events
#-------------------------------------------------------------- 
# Set up the process
process_dir = new_process(process)
# Set up the run card (run_card.dat)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)
# Set up the parameter card (param_card.dat)
modify_param_card(process_dir=process_dir,params={'MASS':masses,'DECAY':decays})
# Generate the events
generate(process_dir=process_dir,runArgs=runArgs)
#Run madspin
#add_madspin(madspin_card=madspin_card, process_dir=process_dir)
# Remember to set saveProcDir to FALSE before sending for production!!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#-------------------------------------------------------------- 
# Shower JOs will go here
#-------------------------------------------------------------- 
if   'yyfusionEE' in yytype: include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_EE_Common.py") 
elif 'yyfusionSD' in yytype: include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_SD_Common.py")
elif 'yyfusionDS' in yytype: include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_DS_Common.py")
elif 'yyfusionDD' in yytype: include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_DD_Common.py")

include("Pythia8_i/Pythia8_ShowerWeights.py")    
include("Pythia8_i//Pythia8_MadGraph.py")
