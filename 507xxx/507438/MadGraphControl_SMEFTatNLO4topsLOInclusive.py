from MadGraphControl.MadGraphUtils import *
# PDF base fragment
import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':315000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[315000], # pdfs for which all variations (error sets) will be included as weights
    'alternative_pdfs':[90400,260000], # pdfs for which only the central set will be included as weights
    'scale_variations':[0.5,1.,2.], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}
from MadGraphControl.MadGraphParamHelpers import *
import fileinput

# --------------------------------------------------------------
# Functions
# --------------------------------------------------------------
# Set SM values changed by model restriction card
# based on https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/MadGraphControl/python/MadGraphParamHelpers.py

def set_SM_params_new(process_dir,FourFS=False):
    """ Set default SM parameters
    Recommended parameter page from PMG:
    https://twiki.cern.ch/twiki/bin/view/AtlasProtected/McProductionCommonParametersMC15
    """
    param_card_settings = {
#    'loop' : { '1':"9.118760e+01" },
    'mass' : {
'5':  "0.000000",
# '15': "1.777000e+00",
'23': "9.118760e+01",
'24': "8.039900e+01",
'25': "1.250000e+02",
        },
    #'yukawa' : { '15': "1.777000e+00 # ymtau" },
    'DECAY' : {
'5'  : """DECAY  5   0.000000e+00""",
# '15' : """DECAY  15   0.000000e+00""",
'23' : """DECAY  23   2.495200e+00""",
'24': """DECAY  24   2.085000e+00
    3.377000e-01   2   -1   2
    3.377000e-01   2   -3   4
    1.082000e-01   2  -11  12
    1.082000e-01   2  -13  14
    1.082000e-01   2  -15  16""",
       }
    }
    if FourFS:
        param_card_settings['mass']['5']="4.950000e+00"
    from MadGraphControl.MadGraphUtils import modify_param_card
    modify_param_card(process_dir=process_dir,params=param_card_settings)


def set_top_params_new(process_dir,mTop=172.5,FourFS=False):
    """ Set default parameters requested by the top group
    This is a convenience helper function
    Recommended parameter page from PMG:
    https://twiki.cern.ch/twiki/bin/view/AtlasProtected/McProductionCommonParametersMC15
    """
    # Set SM parameters
    set_SM_params_new(process_dir,FourFS)
    # Set Higgs parameters
    set_higgs_params_new(process_dir)
    # Calculate the top width based on the mass
    # From https://gitlab.cern.ch/atlasphys-top/reco/MC/blob/master/MCinfo/get_t_width.py
    import math
    # ATLAS MC11 conventions == PDG2010 
    # Vtb=0.999152 
    # using Vtb=1.0 since part of the inputs was already produced using this approximation 
    Vtb=1.0
    M_W=80.399
    # PDG2010 
    G_F=1.16637*(math.pow(10,-5))
    # MSbar alpha_s(mt)
    alpha_s=0.108
    # Born gamma coeff. 
    C1=G_F/(8*math.pi*math.sqrt(2))
    # Born approximation (taking intermediate W-boson to be on-shell) [1]
    wTop_B=C1*math.pow(float(mTop),3)*math.pow(Vtb,2)*pow((1-math.pow((M_W/float(mTop)),2)),2)*(1+2*pow((M_W/float(mTop)),2))
    # EW and QCD corrections to Born: QCD dominates, EW can be neglected [1],[2],[3]
    wTop=wTop_B*(1-0.81*alpha_s-1.81*pow(alpha_s,2))

    param_card_settings = {
    'mass' : { '6':  str(mTop) },
    'yukawa' : { '6': "1.725000e+02 # ymt" },

    'DECAY' : { '6'  : """DECAY  6   """+str(wTop)+"""
    1.000000e+00   2    5  24 # 1.32""",
        },
    }
    from MadGraphControl.MadGraphUtils import modify_param_card
    modify_param_card(process_dir=process_dir,params=param_card_settings)


def set_higgs_params_new(process_dir):
    """ Set Higgs mass and decays
    BR from the Higgs XSec working group for a 125.0 GeV Higgs
    https://twiki.cern.ch/twiki/pub/LHCPhysics/LHCHXSWGBRs/BR.central.dat
    """
    param_card_settings = {
     'MASS' : { '25': "1.250000e+02" },
     'DECAY': { '25': """DECAY  25   6.382339e-03
     5.767E-01 2   5  -5 # H->bb
     6.319E-02 2  15 -15 # H->tautau
     2.192E-04 2  13 -13 # H->mumu
     2.462E-04 2   3  -3 # H->ss
     2.911E-02 2   4  -4 # H->cc
     8.569E-02 2  21  21 # H->gg
     2.277E-03 2  22  22 # H->gammagamma
     1.539E-03 2  22  23 # H->Zgamma
     2.146E-01 2  24 -24 # H->WW
     2.641E-02 2  23  23 # H->ZZ""" }
     }
    from MadGraphControl.MadGraphUtils import modify_param_card
    modify_param_card(process_dir=process_dir,params=param_card_settings)


# --------------------------------------------------------------
# Run options
# --------------------------------------------------------------

runReweighting = 1
runPythia8     = 1
runSetScales   = 1 # sets dynamical scale
runMadSpin     = 0
topdecay       = "decay t > w+ b, w+ > all all \ndecay t~ > w- b~, w- > all all \n"
bwcut          = 15 # MadSpin
increaseWorker = 10

# --------------------------------------------------------------
# Metadata 
# --------------------------------------------------------------
evgenConfig.contact = [ 'mathis.kolb@cern.ch' ]
# evgenConfig.nEventsPerJob = 10000

# --------------------------------------------------------------
# Grid run: create dummy file: remove multicore-mode suffix from file name outputEVNTFile
# --------------------------------------------------------------
if runPythia8:
    pSuffix = runArgs.outputEVNTFile.find("_000")
else:
    pSuffix = 0

# --------------------------------------------------------------
# Setting up the process 
# --------------------------------------------------------------

if process=="tttt":
    process_string = '''
import model SMEFTatNLO-LO_QQQQ
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+ ta+
define vl = ve vm vt
define l- = e- mu- ta-
define vl~ = ve~ vm~ vt~
define allW = l+ vl l- vl~ j
generate p p > t t~ t t~ QCD=4 QED=2 NP=2, ( t~ > b~ allW allW QCD=0 QED=2 NP=0 ) , ( t > b allW allW QCD=0 QED=2 NP=0 )
output -f'''
else:
    raise RuntimeError("process not found")


process_dir = new_process(process_string)

# --------------------------------------------------------------
# run_card
# --------------------------------------------------------------
#Fetch default LO run_card.dat and set parameters
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
# nevents = 2.5* nevents # account for filter efficiency
settings = { 'lhe_version'            :'3.0',
             'cut_decays'             :'F',
             'nevents'                :int(nevents),
             'dynamical_scale_choice' : 0, # 0: user defined dynamical scale
             'bwcutoff'               : '15.0' #     ! (M+/-bwcutoff*Gamma)
}

if not runSetScales:
    settings.pop('dynamical_scale_choice', None)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

#---------------------------------------------------------------------------
# Cook the setscales file for the user defined dynamical scale
#---------------------------------------------------------------------------

if runSetScales:
    fileN = process_dir+'/SubProcesses/setscales.f'
    mark  = '      elseif(dynamical_scale_choice.eq.0) then'
    rmLines = [
    'ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
               'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
               'cc      to use this code you must set                                            cc',
               'cc                 dynamical_scale_choice = 0                                    cc',
               'cc      in the run_card (run_card.dat)                                           cc',
               'write(*,*) "User-defined scale not set"',
               'stop 1',
               'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
               'tmp = 0',
               'cc      USER-DEFINED SCALE: END OF USER CODE                                     cc',
               'stop 21',
               'rscale = 0'
               ]
    
    flag=0
    for line in fileinput.input(fileN, inplace=1):
        toKeep = True
        for rmLine in rmLines:
            if line.find(rmLine) >= 0:
               toKeep = False
               break
        if toKeep:
            print line,
        if line.startswith(mark) and flag==0:
            flag +=1
            print """
c         sum of the transverse mass divide by 4
c         m^2+pt^2=p(0)^2-p(3)^2=(p(0)+p(3))*(p(0)-p(3))
          rscale=0d0
          do i=3,nexternal
            rscale=rscale+dsqrt(max(0d0,(P(0,i)+P(3,i))*(P(0,i)-P(3,i))))
          enddo
          rscale=rscale/4d0
     """

# --------------------------------------------------------------
#  param_card
# --------------------------------------------------------------
# Set ATLAS SM parameters
set_top_params_new(process_dir,mTop=172.5,FourFS=False)

# Set SMEFT@NLO parameters
params             = dict() 
params['dim64f']   = dict()
#params['loop']     = dict()
params['renor']    = dict()
mueft = 1.5 * 1.725000e+02
mur   = mueft
#params['loop']['1']  = str(mur) # not really neeeded if dynamical scale choice used
params['renor']['1'] = str(mueft) # mueft has no effect at LO

# set EFT operator here
cQQ8_base = 0 # always zero, is linear combination of cQQ1
cQQ1_base = 4
cQt1_base = 3
ctt1_base = 2
cQt8_base = 6
params['dim64f']['19'] = str(cQQ8_base)
params['dim64f']['20'] = str(cQQ1_base)
params['dim64f']['21'] = str(cQt1_base)
params['dim64f']['23'] = str(ctt1_base)
params['dim64f']['25'] = str(cQt8_base)


modify_param_card(process_dir=process_dir,params=params)

# --------------------------------------------------------------                                                                  
#  generate reweight parameters                                               
# -------------------------------------------------------------- 
cQQ8_param = []
cQQ1_param = []
cQt1_param = []
ctt1_param = []
cQt8_param = []

opt_name   = []

optNameFormat="cQQ8_{:.1f}_cQQ1_{:.1f}_cQt1_{:.1f}_ctt1_{:.1f}_cQt8_{:.1f}"
countRwgt = 0
for i_cQQ8 in [0]:
    for i_cQQ1 in [0, 0.25, 0.5, 1, 2]:
        for i_cQt1 in [0, 0.25, 0.5, 1, 2]:
            for i_ctt1 in [0, 0.25, 0.5, 1, 2]:
                for i_cQt8 in [0, 0.25, 0.5, 1, 2]:
                    keepRwgt = False
                    testValue = i_cQQ8**2 + i_cQQ1**2 + i_cQt1**2 + i_ctt1**2 + i_cQt8**2
                    if 0.5 < testValue < 16.5:
                        keepRwgt = True
                    keepRwgt = True # all reweighting points are kept
                    if keepRwgt:
                        cQQ8_param.append(i_cQQ8) # not really needed
                        cQQ1_param.append(i_cQQ1 * cQQ1_base)
                        cQt1_param.append(i_cQt1 * cQt1_base)
                        ctt1_param.append(i_ctt1 * ctt1_base)
                        cQt8_param.append(i_cQt8 * cQt8_base)
                        #print(countRwgt, testValue, optNameFormat.format(cQQ8_param[-1], cQQ1_param[-1], cQt1_param[-1], ctt1_param[-1], cQt8_param[-1])) # FIXME remove print statement
                        opt_name.append(optNameFormat.format(cQQ8_param[-1], cQQ1_param[-1], cQt1_param[-1], ctt1_param[-1], cQt8_param[-1]))
                        countRwgt += 1
if runReweighting:
    reweight_card = process_dir + '/Cards/reweight_card.dat'
    reweight_card_f = open(reweight_card, 'w')
    reweight_card_f.write("change helicity True\n")
    for optionRwgt in range(countRwgt):
        reweight_card_f.write("launch --rwgt_info={}\n".format(opt_name[optionRwgt]))
        reweight_card_f.write("       set DIM64F 19 {:.1f}\n".format(cQQ8_param[optionRwgt]))
        reweight_card_f.write("       set DIM64F 20 {:.1f}\n".format(cQQ1_param[optionRwgt]))    
        reweight_card_f.write("       set DIM64F 21 {:.1f}\n".format(cQt1_param[optionRwgt]))    
        reweight_card_f.write("       set DIM64F 23 {:.1f}\n".format(ctt1_param[optionRwgt]))    
        reweight_card_f.write("       set DIM64F 25 {:.1f}\n".format(cQt8_param[optionRwgt]))    
    reweight_card_f.close()
    
    print_cards()

#---------------------------------------------------------------------------
# MadSpin Card
#---------------------------------------------------------------------------

if runMadSpin:
    madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
    mscard = open(madspin_card_loc,'w')
    mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#set Nevents_for_max_weigth 75 # FIXME is it needed to set it to higher value?
set BW_cut %i
set seed %i
%s
launch
"""%(bwcut, runArgs.randomSeed, topdecay))
    mscard.close()

# --------------------------------------------------------------
# Generate
# --------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)  
evgenConfig.generators = ["MadGraph"]

# --------------------------------------------------------------
# Run Pythia 8 Showering
# --------------------------------------------------------------
evgenConfig.description = 'MadGraph 4tops SMEFTatNLO with reweighting' 
evgenConfig.keywords+=['4top'] 

if runPythia8:
    # Helper for resetting process number
    check_reset_proc_number(opts)
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
    include("Pythia8_i/Pythia8_MadGraph.py")

