include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa ttW-EWK LO"
evgenConfig.keywords = ["SM", "top", "ttbar", "2lepton" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "narayan@cern.ch" ]

genSeq.Sherpa_i.RunCard="""
(run){
    %scales, tags for scale variations
    FSF:=1.; RSF:=1.; QSF:=1.;
    SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
    CORE_SCALE QCD;
    EXCLUSIVE_CLUSTER_MODE 1;
    METS_BBAR_MODE=5
    NLO_CSS_PSMODE=1

    %tags for process setup
    NJET:=1; QCUT:=30.;

    %me generator settings
    ME_SIGNAL_GENERATOR Comix;
    INTEGRATION_ERROR=0.05;

    %decay settings
    HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
    STABLE[24] 0; STABLE[6] 0; WIDTH[6] 0;

    SPECIAL_TAU_SPIN_CORRELATIONS=1
    SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
    Process 93 93 -> 6 -6 24 93{NJET};
    Order (*,3);
    CKKW sqr(QCUT/E_CMS);
    End process;

    Process 93 93 -> 6 -6 -24 93{NJET};
    Order (*,3);
    CKKW sqr(QCUT/E_CMS);
    End process;

}(processes)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

genSeq.Sherpa_i.NCores = 24
evgenConfig.nEventsPerJob = 10000
