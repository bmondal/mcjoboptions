#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125" ]

evgenConfig.description = "POWHEG+Pythia8 H+Z+jet->l+l-bbbarbbbar production"
evgenConfig.process     = "hSM->h1h2->4b, Z->ll or W->lnu"
evgenConfig.contact     = [ 'lars.rickard.stroem@cern.ch' ]

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

#--------------------------------------------------------------
# Defining the function to extract parameters
#--------------------------------------------------------------
mh1 = 20
wh1 = 0.00407
mh2 = 20
wh2 = 0.00407
mh3 = 125.
wh3 = 0.00407

# Setting up variables
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
evgenLog.info("phys_short = {:s}".format(phys_short))
data = phys_short.split("_")
shower = data[0]
model = data[1]
if model.startswith("asym"):
    evgenLog.info("Asymmetric sample requested!")
    mh1 = float(data[3])
    wh1 = 0.00407
    mh2 = float(data[5])
    wh2 = 0.00407
    mh3 = 125.
    wh3 = 0.00407
    process = data[8]
    if (mh1+mh2)>mh3: evgenLog.error("The sum of mh1+mh2 exceeds the Higgs mass! Not possible.")
    if not (mh1 <= mh2 < mh3): evgenLog.error("Mass hierarchy not correct mh1 (mh3) must be lightest (heaviest)!")
elif model == "sym":
    evgenLog.info("Symmetric sample requested!")
    mh1 = float(data[3])
    wh1 = 0.00407
    mh2 = float(data[3])
    wh2 = 0.00407
    mh3 = 125.
    wh3 = 0.00407
    process = data[4]

# Printing some settings
evgenLog.info("mh1 = %.2f"%mh1)
evgenLog.info("wh1 = %.6f"%wh1)
evgenLog.info("mh2 = %.2f"%mh2)
evgenLog.info("wh2 = %.6f"%wh2)
evgenLog.info("mh3 = %.2f"%mh3)
evgenLog.info("wh3 = %.6f"%wh3)

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
if 'Py8EG' not in shower:
    evgenLog.error('I only know how to do Pythia AZNLO shower, sorry')

include("Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py")
if 'Var1Down' in shower:
    genSeq.Pythia8.Commands += [
        "Tune:pp = 5",
        "BeamRemnants:primordialKThard = 1.780",
        "SpaceShower:alphaSorder = 2",
        "SpaceShower:alphaSvalue = 0.118",
        "SpaceShower:pT0Ref = 1.928",
        "MultipartonInteractions:pT0Ref = 2.002887"
    ]
elif 'Var1Up' in shower:
    genSeq.Pythia8.Commands += [
        "Tune:pp = 5",
        "BeamRemnants:primordialKThard = 1.719",
        "SpaceShower:alphaSorder = 2",
        "SpaceShower:alphaSvalue = 0.118",
        "SpaceShower:pT0Ref = 1.919",
        "MultipartonInteractions:pT0Ref = 2.002887"
    ]
elif 'Var2Down' in shower:
    genSeq.Pythia8.Commands += [
        "Tune:pp = 5",
        "BeamRemnants:primordialKThard = 1.737",
        "SpaceShower:alphaSorder = 2",
        "SpaceShower:alphaSvalue = 0.118",
        "SpaceShower:pT0Ref = 2.004",
        "MultipartonInteractions:pT0Ref = 2.002887"
    ]
elif 'Var2Up' in shower:
    genSeq.Pythia8.Commands += [
        "Tune:pp = 5",
        "BeamRemnants:primordialKThard = 1.762",
        "SpaceShower:alphaSorder = 2",
        "SpaceShower:alphaSvalue = 0.118",
        "SpaceShower:pT0Ref = 1.844",
        "MultipartonInteractions:pT0Ref = 2.002887"
    ]
elif 'rFactDown' in shower:
    genSeq.Pythia8.Commands += [ "StringZ:rFactB = 0.855" ]
elif 'rFactUp' in shower:
    genSeq.Pythia8.Commands += [ "StringZ:rFactB = 1.050" ]

include("Pythia8_i/Pythia8_Powheg_Main31.py")
if process == "ggZhemt":
    genSeq.Pythia8.Commands += ['POWHEG:nFinal = 2']
else:
    genSeq.Pythia8.Commands += ["POWHEG:nFinal = 3"]

genSeq.Pythia8.Commands += [
    'Higgs:useBSM = on',
    '35:m0 = %.1f' % mh3,
    '35:mWidth = %.6f' % wh3,
    '35:doForceWidth = on',
]

if model.startswith("asym"):

    if mh2 == 2.*mh1:
        evgenLog.info("Test mode activated: generation of symmetric decays with model setting 'asym'.")
        evgenLog.info("No open decay channel for id=35, masses (mh1=%.2f,mh2=%.2f) need to be adjusted slightly."%(mh1,mh2))
        diff = 0.005*mh1 #0.5% of mh1
        mh1 -= diff
        mh2 += diff
        evgenLog.warning("Adjusting masses (+-5 percent of mh1), new mh1 = %.2f"%mh1)
        evgenLog.warning("Adjusting masses (+-5 percent of mh1), new mh2 = %.2f"%mh2)

    genSeq.Pythia8.Commands += [
        'HiggsH2:coup2A3H1 = 1',
        '35:oneChannel = 1 1.0 0 25 36',
    ]
    
    genSeq.Pythia8.Commands += [
        '25:oneChannel = 1 1.0 0 5 -5',
        '25:m0 = %.1f' % mh1,
        '25:mWidth = %.6f' % wh1,
        '25:doForceWidth = on',
        '25:mMin = 0',
        '25:tau0 = 0',
    ]
    
    if mh2 < 2*mh1:
        #Direct decay
        genSeq.Pythia8.Commands += [
            '36:oneChannel = 1 1.0 0 5 -5',
            '36:m0 = %.1f' % mh2,
            '36:mWidth = %.6f' % wh2,
            '36:doForceWidth = on',
            '36:mMin = 0',
            '36:tau0 = 0',
        ]
    else:
        #Single cascade decay
        genSeq.Pythia8.Commands += [
            'HiggsA3:coup2H1H1 = 1',
            '36:onMode = off',
            '36:oneChannel = 1 1.0 0 25 25',
            '36:m0 = %.1f' % mh2,
            '36:mWidth = %.6f' % wh2,
            '36:doForceWidth = on',
            '36:mMin = 0',
            '36:tau0 = 0',
        ]

elif model == "sym":

    genSeq.Pythia8.Commands += [
        '35:oneChannel = 1 1.0 0 36 36',
    ]
    
    genSeq.Pythia8.Commands += [
        '36:oneChannel = 1 1.0 0 5 -5',
        '36:m0 = %.1f' % mh1,
        '36:mWidth = %.6f' % wh1,
        '36:doForceWidth = on',
        '36:mMin = 0',
        '36:tau0 = 0',
    ]
