###
### This JO file executes following tasks:
### 1) It generates NLO ttbar events
### 2) It decays one top/antitop-quark into a sam-sign dilepton final state with a HNL particle resonance in decay chain
### 3) The other top/antitop-quark decays according SM
### 4) The decays are then showered with Pythia8
### For details please check the following links or contact the authors
### - https://gitlab.cern.ch/ooncel/hnl-signal-generation
### - 
### 

import os
#import inspect
import fileinput
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment #Importing ATLAS recommended PDF settings
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
tokens = get_physics_short().split('_')

###Parse JO name to set HNL mass and sign of the same-sign lepton pair
HeavyNMass  = str(tokens[6])
SS2L_sign   = str(tokens[5])

###General settings

###Scale up number of events so Pythia won't run out of events later on
###When applying filter this needs to be increased depending on the filter efficiency
###Default Pythia overlap removal filter requires we demand 10-20% more events
###in order not to run out of events as Pythia executes showering
nevents=runArgs.maxEvents*1.1 if runArgs.maxEvents > 0 else 10000

###MadSpin Parameters
###They are currently not used as defaults (see Launch command for MadSpin below)
bwcut=15
nevents_maxw=400
maxw_points=400

###Generation Settings
mode=2 #0 single machine, 1 cluster, 2 multicore
nJobs=5
gridpack_dir=None
gridpack_mode=False
cluster_type=None # 'condor'
cluster_queue=None # '15'

###NLO ttbar event generation syntax
required_accuracy = 0.001
process="""
import model SM_HeavyN_NLO
define p = g u c d s b u~ c~ d~ s~ b~
define j = p
define ssl+ = e+ mu+
define ssl- = mu- e-
define l+ = e+ mu+ ta+
define l- = mu- e- ta-
define vl = vm ve vt
define vl~ = vm~ ve~ vt~
define n = n1 n2
define all_sm = j l+ l- vl vl~
generate p p > t t~ [QCD] 
output -f
    """
###We have two different decay chains depending on the sign of same-sign leptons

###(plus,plus)
if SS2L_sign=='p':
    ssDecayMode= 'l+l+'
    ttdecay = "decay t > b w+, (w+ > n ssl+ , n > ssl+ j j) \n decay t~ > b~ w-, w- > all_sm all_sm NP=0 \n"
###(minus,minus)
elif SS2L_sign=='m':
    ssDecayMode= 'l-l-'
    ttdecay = "decay t > b w+, (w+ > all_sm all_sm) NP=0 \n decay t~ > b~ w-, (w- > n ssl-, n > ssl- j j) \n"
else:
    raise RuntimeError("JO parsing %s is not recognised for this control files."%(SS2L_sign))

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(process)

### Custom scale used for ttbar official sample
### Cook the setscales file for the user defined dynamical scale
fileN = process_dir+'/SubProcesses/setscales.f'
mark  = '      elseif(dynamical_scale_choice.eq.10) then'
rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
           'cc      to use this code you must set                                            cc',
           'cc                 dynamical_scale_choice = 0                                    cc',
           'cc      in the run_card (run_card.dat)                                           cc',
           'write(*,*) "User-defined scale not set"',
           'stop 1',
           'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
           'tmp = 0',
           'cc      USER-DEFINED SCALE: END OF USER CODE                                     cc'
           ]

for line in fileinput.input(fileN, inplace=1):
    toKeep = True
    for rmLine in rmLines:
        if line.find(rmLine) >= 0:
           toKeep = False
           break
    if toKeep:
        print line,
    if line.startswith(mark):
        print """
c         sum of the transverse mass divide by 2
c         m^2+pt^2=p(0)^2-p(3)^2=(p(0)+p(3))*(p(0)-p(3))
          tmp=0d0
          do i=3,nexternal
            tmp=tmp+dsqrt(max(0d0,(pp(0,i)+pp(3,i))*(pp(0,i)-pp(3,i))))
          enddo
          tmp=tmp/4d0
          temp_scale_id=\'H_T/4 := sum_i mT(i)/4, i=final state\'"""

# HT/4 from 4tops
# print """
#c         sum of the transverse mass divide by 2
#c         m^2+pt^2=p(0)^2-p(3)^2=(p(0)+p(3))*(p(0)-p(3))
#          tmp=0d0
#          do i=3,nexternal
#            tmp=tmp+dsqrt(max(0d0,(pp(0,i)+pp(3,i))*(pp(0,i)-pp(3,i))))
#          enddo
#          tmp=tmp/4d0
#          temp_scale_id=\'H_T/4 := sum_i mT(i)/4, i=final state\'"""
#

# Official ttbar scale
#c         Q^2= mt^2 + 0.5*(pt^2+ptbar^2)
#          xm2=dot(pp(0,3),pp(0,3))
#          tmp=sqrt(xm2+0.5*(pt(pp(0,3))**2+pt(pp(0,4))**2))
#          temp_scale_id='mt**2 + 0.5*(pt**2+ptbar**2)'
# 
 
 
 

###Following list of changes will be modified in the default HNL run_card
extras_runcard = {
    'nevents'      	    : int(nevents),
    'dynamical_scale_choice':10,
    'parton_shower'	    :"'PYTHIA8'",
    'jetalgo'      	    :-1,
    'jetradius'    	    :0.4,
    'ptj'	   	    :0.1,
    'ickkw'        	    : 0,
    'maxjetflavor' 	    : 5,
}

###Modify the run_card by applying the extras
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras_runcard)

###Following list of changes will be modified in the default HNL param_card
extras_paramcard = {
    'SMINPUTS' :{
        'Gf'   :0.0000116637,
        'aEWM1':132.348905,
	'aS'   :1.18400e-01, # aS
    },
    'decay'  :{
        'WN1':"Auto",
        'WN2':"Auto",
	# Uncomment if you want LO top width
	'6'  : """DECAY  6            1.32
    		1.000000e+00   2    5  24 # 1.32""",
        '24' : """DECAY  24   2.085000e+00
     		3.377000e-01   2   -1   2
     		3.377000e-01   2   -3   4
     		1.082000e-01   2  -11  12
     		1.082000e-01   2  -13  14
     		1.082000e-01   2  -15  16""",
	'25': """DECAY  25   6.382339e-03
    		 5.767E-01 2   5  -5 # H->bb
    		 6.319E-02 2  15 -15 # H->tautau
    		 2.192E-04 2  13 -13 # H->mumu
    		 2.462E-04 2   3  -3 # H->ss
    		 2.911E-02 2   4  -4 # H->cc
    		 8.569E-02 2  21  21 # H->gg
    		 2.277E-03 2  22  22 # H->gammagamma
    		 1.539E-03 2  22  23 # H->Zgamma
    		 2.146E-01 2  24 -24 # H->WW
    		 2.641E-02 2  23  23 # H->ZZ""" 
    },
    'yukawa':{
       '6'  : "1.725000e+02 # ymt",
    },
    'mass'   :{
        'mN1':HeavyNMass,
        'mN2':HeavyNMass,
	'6'  : "172.5",
	'24' : "8.039900e+01",
	'25' : "1.250000e+02",
    },
    'numixing' :{
        'VeN1' :1,
        'VeN2' :0,
        'VeN3' :0,
        'VmuN1':0,
        'VmuN2':1,
        'VmuN3':0,
        'VtaN1':0,
        'VtaN2':0,
        'VtaN3':0, # No taus
    }
}

###Use HNL default param_card but now modify it by applying the changes made above
modify_param_card(process_dir=process_dir,params=extras_paramcard)

print_cards()

###Adding MadSpin Card Generation
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')                                                                                                                                    
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
                                                                                                          
set spinmode none
set seed %i
%s
# running the actual code
launch"""%(runArgs.randomSeed,ttdecay))                                                                                                                                              
# if you want to customize these values use below line instead of above line to fetch values
#launch"""%(bwcut,nevents_maxw,maxw_points,runArgs.randomSeed,ttdecay))      

mscard.close()

###Following command executes the generation process
generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs,required_accuracy=required_accuracy)

### Output is formatted, including enforcing LHE3 format
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,saveProcDir=True,lhe_version=3)

###Unlike Madgraph, Pythia cannot run on multi-core mode, go back to single core before executing showering
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    nJobs = os.environ.pop('ATHENA_PROC_NUMBER')
    if not hasattr(opts,'nprocs'):
        print 'Did not see option!'
    else:
        opts.nprocs = 0
    print opts.nprocs

###Shower
runArgs.inputGeneratorFile=outputDS

evgenConfig.description = 'aMC@NLO, Pythia8, ttbar -> HNL (m='+HeavyNMass+'GeV) -> '+ssDecayMode
evgenConfig.keywords+=['BSM','neutrino','multilepton','5flavour','NLO','sameSign','ttbar'] 
evgenConfig.contact = ["Mario Jose Sousa (SDU) <mdacunha@cern.ch>","Ogul Oencel (Bonn) <ogul.oncel@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")  #Import recommended Pythia Tunes

###Add filters below when needed
#include("GeneratorFilters/TTbarWToLeptonFilter.py")
#filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
#filtSeq.TTbarWToLeptonFilter.Ptcut      = 0.0

print("=============================")
print("=============================")
print("HeavyNMass="+HeavyNMass)
print("SS2L_sign="+SS2L_sign)
print("=============================")
print("=============================")


